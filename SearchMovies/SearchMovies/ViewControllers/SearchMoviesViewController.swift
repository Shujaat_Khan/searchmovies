//
//  SearchMoviesViewController.swift
//  Tobdon
//
//  Created by Shujaat Ali Khan on 9/21/19.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit

class SearchMoviesViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    var searchController: UISearchController!
    
    var recentSearch:[RecentSearch] = []
    var movieData:[MovieResult] = []
    var searchText:String = ""
    
    var currentPage = 1
    var shouldShowLoadingCell = false

    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Search Movies"
        
        self.setStatusBarStyle(.lightContent)
        
        self.hideKeyboardWhenTappedAround()
        setUpTableView()
        configureSearchController()
    }
    
    
    // MARK: - Private Methods
    private func setUpTableView() {
        tableView.registerCell(MovieTableViewCell.self)
        tableView.registerCell(RecentSearchTableViewCell.self)
        tableView.registerCell(LoadMoreCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        tableView.delegate = self
        tableView.dataSource = self
        
        recentSearch = DBManager.sharedInstance.getRecentsearch()
    }
    
    private func configureSearchController() {
        // Initialize and configuration to the search controller.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.barTintColor = UIColor.darkGray
        searchController.searchBar.placeholder = "Search Movies..."
        searchController.searchBar.delegate = self
        
        searchController.searchBar.tintColor = UIColor.init(red: 0, green: 254/255, blue: 194/255, alpha: 1.0)
 
        // Add search bar to the tableview headerview.
        tableView.tableHeaderView = searchController.searchBar
        
    }
    
    private func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty() && movieData.count > 0
    }

    
    //MARK:- APIS
    func getMovie() {
        if !Internet.checkInternet() {
            return
        }
        
        let params:[String:Any] = ["api_key":Route.searchMovieApiKey,"query":searchText,"page":currentPage]
        
        MovieServiceManager().searchMovieApi(param: params, isLoader: !shouldShowLoadingCell) { [unowned self] (data) in
            
            if self.shouldShowLoadingCell {
                self.movieData += data.results ?? []
            }else{
                self.movieData = data.results ?? []
            }
            
            self.shouldShowLoadingCell = self.currentPage == data.totalPages ? false:true
            
            self.searchController.isActive = true
    
            self.tableView.reloadData()
            
            DBManager.sharedInstance.addRecentSearch(title: self.searchText)
        }
    }

}

//MARK:- TableView DataSource & Delegate
extension SearchMoviesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if isFiltering() {
            return shouldShowLoadingCell ? movieData.count+1:movieData.count
        }else{
            return recentSearch.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isFiltering() {
            
            if indexPath.row == movieData.count && shouldShowLoadingCell {
                //Show pagging loading cell
                let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreCell.identifier()) as! LoadMoreCell
                return cell
            }else{
                //Load Movies data
                let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.identifier(), for: indexPath) as! MovieTableViewCell
                cell.setData(movieData[indexPath.row])
                return cell
            }
        } else {
            //Load Recent Searches
            let cell = tableView.dequeueReusableCell(withIdentifier: RecentSearchTableViewCell.identifier(), for: indexPath) as! RecentSearchTableViewCell
            
            cell.setData(recentSearch[indexPath.row])
            
            return cell
        }
        
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return isFiltering() ? nil:"Recent Searches"
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.backgroundView?.backgroundColor = UIColor.darkGray
        header.textLabel?.font = UIFont.systemFont(ofSize: 14)
        header.textLabel?.textColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == movieData.count && shouldShowLoadingCell {
            if Internet.isConnected {
                currentPage += 1
                getMovie()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isFiltering() {
            searchController.searchBar.text = recentSearch[indexPath.row].title
            searchText = recentSearch[indexPath.row].title
            getMovie()
        }
        
    }
}

// MARK: - UISearchController Delegate
extension SearchMoviesViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        if searchBarIsEmpty() {
            resetSearch()
            recentSearch = DBManager.sharedInstance.getRecentsearch()
            tableView.reloadData()
            
            if recentSearch.count > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .none, animated: false)
            }

        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !searchBarIsEmpty() {
            resetSearch()
            searchText = searchBar.text ?? ""
            getMovie()
        }
    }
    
    func resetSearch(){
        currentPage = 1
        shouldShowLoadingCell = false
        movieData.removeAll()
    }
}
// Hide Keyboard on tap any where
extension SearchMoviesViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.searchController.searchBar.endEditing(true)
    }
}
// Dismis keyboard on scrolling
extension SearchMoviesViewController:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         self.searchController.searchBar.endEditing(true)
    }
}
