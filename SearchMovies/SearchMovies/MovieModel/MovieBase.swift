//
//	MovieBase.swift


import Foundation

class MovieBase : Codable {

	let page : Int?
	let results : [MovieResult]?
	let totalPages : Int?
	let totalResults : Int?


	enum CodingKeys: String, CodingKey {
		case page = "page"
		case results = "results"
		case totalPages = "total_pages"
		case totalResults = "total_results"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		page = try values.decodeIfPresent(Int.self, forKey: .page) ?? Int()
		results = try values.decodeIfPresent([MovieResult].self, forKey: .results) ?? [MovieResult]()
		totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages) ?? Int()
		totalResults = try values.decodeIfPresent(Int.self, forKey: .totalResults) ?? Int()
	}


}


