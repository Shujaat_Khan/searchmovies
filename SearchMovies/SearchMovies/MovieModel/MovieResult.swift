//
//	MovieResult.swift

import Foundation

class MovieResult : Codable {

	let adult : Bool?
	let backdropPath : String?
	let genreIds : [Int]?
	let id : Int?
	let originalLanguage : String?
	let originalTitle : String?
	let overview : String?
	let popularity : Float?
	let posterPath : String?
	let releaseDate : String?
	let title : String?
	let video : Bool?
	let voteAverage : Float?
	let voteCount : Int?


	enum CodingKeys: String, CodingKey {
		case adult = "adult"
		case backdropPath = "backdrop_path"
		case genreIds = "genre_ids"
		case id = "id"
		case originalLanguage = "original_language"
		case originalTitle = "original_title"
		case overview = "overview"
		case popularity = "popularity"
		case posterPath = "poster_path"
		case releaseDate = "release_date"
		case title = "title"
		case video = "video"
		case voteAverage = "vote_average"
		case voteCount = "vote_count"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		adult = try values.decodeIfPresent(Bool.self, forKey: .adult) ?? Bool()
		backdropPath = try values.decodeIfPresent(String.self, forKey: .backdropPath) ?? String()
		genreIds = try values.decodeIfPresent([Int].self, forKey: .genreIds) ?? [Int]()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		originalLanguage = try values.decodeIfPresent(String.self, forKey: .originalLanguage) ?? String()
		originalTitle = try values.decodeIfPresent(String.self, forKey: .originalTitle) ?? String()
		overview = try values.decodeIfPresent(String.self, forKey: .overview) ?? String()
		popularity = try values.decodeIfPresent(Float.self, forKey: .popularity) ?? Float()
		posterPath = try values.decodeIfPresent(String.self, forKey: .posterPath) ?? String()
		releaseDate = try values.decodeIfPresent(String.self, forKey: .releaseDate) ?? String()
		title = try values.decodeIfPresent(String.self, forKey: .title) ?? String()
		video = try values.decodeIfPresent(Bool.self, forKey: .video) ?? Bool()
		voteAverage = try values.decodeIfPresent(Float.self, forKey: .voteAverage) ?? Float()
		voteCount = try values.decodeIfPresent(Int.self, forKey: .voteCount) ?? Int()
	}


}
