//
//  RecentSearch.swift


import Foundation
import RealmSwift

class RecentSearch: Object {
    @objc dynamic var id = 0
    @objc dynamic var title = ""
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(RecentSearch.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
