//
//  MovieTableViewCell.swift
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgViewPoster: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data:MovieResult) {
        imgViewPoster.setImageFromUrl(urlStr: data.posterPath ?? "", placeholder: "placeholder-poster")
        lblTitle.text = data.title
        lblReleaseDate.text = data.releaseDate == "" ? "N/A":data.releaseDate
        lblOverview.text = data.overview
    }
    
}
