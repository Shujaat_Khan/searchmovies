//
//  RecentSearchTableViewCell.swift


import UIKit

class RecentSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(_ data:RecentSearch){
        lblTitle.text = data.title
    }

    
}
