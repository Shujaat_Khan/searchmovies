//
//  DBManager.swift


import UIKit
import RealmSwift

class DBManager {
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
    func getRecentsearch() -> [RecentSearch] {
        let results: Results<RecentSearch> =   database.objects(RecentSearch.self).sorted(byKeyPath: "id", ascending: false)
        let searches = Array(results)
        return searches
    }
    
    func addRecentSearch(title: String)   {

        let items = database.objects(RecentSearch.self)
        if items.filter("title = '\(title)'").count == 0 && title != "" {

            if items.count == 10 {
                deleteFirstSearchFromDb()
            }

            let object = RecentSearch()
            object.title = title
            object.id = object.incrementID()
            
            try! database.write {
                database.add(object)
                print("Added new object")
            }

        }
        
    }

    func deleteFirstSearchFromDb()   {
        if let firstItem = database.objects(RecentSearch.self).sorted(byKeyPath: "id", ascending: true).first {
            try! database.write {
                database.delete(firstItem)
                
            }
        }
    }
}
