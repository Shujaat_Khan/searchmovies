//
//  MovieServiceManager.swift


import Foundation
import Alamofire

class MovieServiceManager {
    
    
    func apiGeneric<T : Decodable> (_ param: AFParam,isLoader:Bool , modelType : T.Type , completion: @escaping (T) -> Void){

        //request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: isLoader, success: { (response) in
            
            guard let data = response else { return }
            
            do{
                let decoder = JSONDecoder()
                let result = try decoder.decode(modelType.self, from: data)
                
                completion(result)
                
            }catch let error{
                print("Error: ", error)
            }
            
        }) { (error) in
            
        }
    }
    
    func searchMovieApi(param:[String:Any],isLoader: Bool,completion: @escaping (MovieBase) -> Void){
        
        let param = AFParam(endpoint: Route.searchMovie, params: param, headers: [:], method: .get, parameterEncoding: URLEncoding.default, images: [])
        
        self.apiGeneric(param, isLoader: isLoader, modelType:MovieBase.self ) { (res) in

            if res.results?.count != 0 {
                completion(res)
            }else{
                Alert.showMsg(title: "Alert", msg: "No Record Found", btnActionTitle: "Ok")
            }

        }
    }
    
    
}




