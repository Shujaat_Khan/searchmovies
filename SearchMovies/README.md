Search Movies App

DESCRIPTION
------------------------------
- App contain search functionality for movies and store recent searches
- Recent searches data store in local database.
- Native UISearchController is use for search functionality 
- Native UITableView is use for movies list


RESOURCES
------------------------------
- Shujaat Ali Khan

ACCOUNTS
------------------------------
No Account credentials required for this app.


VERSION REQUIREMENTS
------------------------------
iOS 9+ and iPhone 5 or above

THIRD PARTY LIBRARIES/ FRAMEWORKS
------------------------------
[PODS]
- Alamofire         (This is use for Http requests)
- RealmSwift      (Realm is use for persistent data storage )
- Kingfisher        (This library use for loading and caching images from the server)


DEVELOPER NOTES
------------------------------
None

KNOWN ISSUES
------------------------------
None

PENDING CHANGE REQUESTS
------------------------------
None


CHANGE HISTORY
------------------------------
None

